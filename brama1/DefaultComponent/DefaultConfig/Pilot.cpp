/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Pilot
//!	Generated Date	: Thu, 21, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Pilot.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Pilot.h"
//## link itsOdbiornik
#include "Odbiornik.h"
//#[ ignore
#define Default_Pilot_Pilot_SERIALIZE OM_NO_OP

#define Default_Pilot_czytajUstawienia_SERIALIZE OM_NO_OP

#define Default_Pilot_zapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));
//#]

//## package Default

//## class Pilot
Pilot::Pilot(IOxfActive* theActiveContext) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Pilot, Pilot(), 0, Default_Pilot_Pilot_SERIALIZE);
    setActiveContext(this, true);
    itsOdbiornik = NULL;
    initStatechart();
}

Pilot::~Pilot() {
    NOTIFY_DESTRUCTOR(~Pilot, false);
    cleanUpRelations();
}

std::string Pilot::czytajUstawienia() {
    NOTIFY_OPERATION(czytajUstawienia, czytajUstawienia(), 0, Default_Pilot_czytajUstawienia_SERIALIZE);
    //#[ operation czytajUstawienia()
      return "";
    //#]
}

bool Pilot::zapiszUstawienia(std::string nastawy) {
    NOTIFY_OPERATION(zapiszUstawienia, zapiszUstawienia(std::string), 1, Default_Pilot_zapiszUstawienia_SERIALIZE);
    //#[ operation zapiszUstawienia(std::string)
     return true;
    //#]
}

Odbiornik* Pilot::getItsOdbiornik() const {
    return itsOdbiornik;
}

void Pilot::setItsOdbiornik(Odbiornik* p_Odbiornik) {
    _setItsOdbiornik(p_Odbiornik);
}

bool Pilot::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Pilot::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Pilot::cleanUpRelations() {
    if(itsOdbiornik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsOdbiornik");
            itsOdbiornik = NULL;
        }
}

void Pilot::__setItsOdbiornik(Odbiornik* p_Odbiornik) {
    itsOdbiornik = p_Odbiornik;
    if(p_Odbiornik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsOdbiornik", p_Odbiornik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsOdbiornik");
        }
}

void Pilot::_setItsOdbiornik(Odbiornik* p_Odbiornik) {
    __setItsOdbiornik(p_Odbiornik);
}

void Pilot::_clearItsOdbiornik() {
    NOTIFY_RELATION_CLEARED("itsOdbiornik");
    itsOdbiornik = NULL;
}

void Pilot::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        //#[ state Wylaczony.(Entry) 
        std::cout << "Pilot::Wylaczony entered! " << std::endl;
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Pilot::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evAktywuj_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    //#[ state Wylaczony.(Exit) 
                    std::cout << "Pilot::Wylaczony exited! " << std::endl;
                    //#]
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    //#[ transition 1 
                     std::cout << "evImpuls" << std::endl;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            if(IS_EVENT_TYPE_OF(evAktywuj_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    //#[ state Wylaczony.(Entry) 
                    std::cout << "Pilot::Wylaczony entered! " << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evImpuls_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    //#[ transition 3 
                    std::cout << "impuls z pilota" << std::endl;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
                    pushNullTransition();
                    rootState_subState = sendaction_2;
                    rootState_active = sendaction_2;
                    //#[ state sendaction_2.(Entry) 
                    itsOdbiornik->GEN(evImpuls);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPilot::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPilot::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsOdbiornik", false, true);
    if(myReal->itsOdbiornik)
        {
            aomsRelations->ADD_ITEM(myReal->itsOdbiornik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedPilot::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Pilot::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Pilot::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Pilot::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPilot::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedPilot::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedPilot::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Pilot, Default, false, Modul, OMAnimatedModul, OMAnimatedPilot)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Pilot.cpp
*********************************************************************/
