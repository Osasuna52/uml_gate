/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Thu, 21, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class Brama;

//## auto_generated
class DetektorKolizji;

//## auto_generated
class Lampa;

//## auto_generated
class Modul;

//## auto_generated
class Naped;

//## auto_generated
class Odbiornik;

//## auto_generated
class Pilot;

//## auto_generated
class Sterownik;

//## auto_generated
class Ustawienia;

//#[ ignore
#define evImpuls_Default_id 18601

#define evValidate_Default_id 18602

#define evDekoduj_Default_id 18603

#define evStop_Default_id 18604

#define evStart_Default_id 18605

#define evKrokRuchu_Default_id 18606

#define evSprawdzKolizje_Default_id 18607

#define evKolizja_Default_id 18608

#define evMigaj_Default_id 18609

#define evPauza_Default_id 18610

#define evAktywuj_Default_id 18611

#define evTest_Default_id 18612

#define evSignal_Default_id 18613

#define evPrzelacz_Default_id 18614
//#]

//## package Default


//## type Stany
enum Stany {
    ZAMYKANIE = -1,
    ZATRZYMANIE = 0,
    OTWIERANIE = 1
};

//## event evImpuls()
class evImpuls : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevImpuls;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evImpuls();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevImpuls : virtual public AOMEvent {
    DECLARE_META_EVENT(evImpuls)
};
//#]
#endif // _OMINSTRUMENT

//## event evValidate()
class evValidate : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevValidate;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evValidate();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevValidate : virtual public AOMEvent {
    DECLARE_META_EVENT(evValidate)
};
//#]
#endif // _OMINSTRUMENT

//## event evDekoduj()
class evDekoduj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDekoduj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDekoduj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDekoduj : virtual public AOMEvent {
    DECLARE_META_EVENT(evDekoduj)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event evStart(Stany)
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
//#[ ignore
    evStart(int p_stan);
//#]

    //## auto_generated
    evStart();
    
    //## auto_generated
    evStart(Stany p_stan);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    Stany stan;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evKrokRuchu(float)
class evKrokRuchu : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKrokRuchu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKrokRuchu();
    
    //## auto_generated
    evKrokRuchu(float p_droga);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    float droga;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKrokRuchu : virtual public AOMEvent {
    DECLARE_META_EVENT(evKrokRuchu)
};
//#]
#endif // _OMINSTRUMENT

//## event evSprawdzKolizje()
class evSprawdzKolizje : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSprawdzKolizje;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSprawdzKolizje();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSprawdzKolizje : virtual public AOMEvent {
    DECLARE_META_EVENT(evSprawdzKolizje)
};
//#]
#endif // _OMINSTRUMENT

//## event evKolizja()
class evKolizja : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKolizja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKolizja();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKolizja : virtual public AOMEvent {
    DECLARE_META_EVENT(evKolizja)
};
//#]
#endif // _OMINSTRUMENT

//## event evMigaj()
class evMigaj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevMigaj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evMigaj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevMigaj : virtual public AOMEvent {
    DECLARE_META_EVENT(evMigaj)
};
//#]
#endif // _OMINSTRUMENT

//## event evPauza()
class evPauza : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPauza;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPauza();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPauza : virtual public AOMEvent {
    DECLARE_META_EVENT(evPauza)
};
//#]
#endif // _OMINSTRUMENT

//## event evAktywuj()
class evAktywuj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAktywuj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAktywuj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAktywuj : virtual public AOMEvent {
    DECLARE_META_EVENT(evAktywuj)
};
//#]
#endif // _OMINSTRUMENT

//## event evTest()
class evTest : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevTest;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evTest();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevTest : virtual public AOMEvent {
    DECLARE_META_EVENT(evTest)
};
//#]
#endif // _OMINSTRUMENT

//## event evSignal()
class evSignal : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSignal;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSignal();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSignal : virtual public AOMEvent {
    DECLARE_META_EVENT(evSignal)
};
//#]
#endif // _OMINSTRUMENT

//## event evPrzelacz()
class evPrzelacz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPrzelacz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPrzelacz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPrzelacz : virtual public AOMEvent {
    DECLARE_META_EVENT(evPrzelacz)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
