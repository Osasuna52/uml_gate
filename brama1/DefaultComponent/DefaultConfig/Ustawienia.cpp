/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Tue, 19, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Ustawienia.h"
//## link itsBrama
#include "Brama.h"
//#[ ignore
#define Default_Ustawienia_Ustawienia_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Ustawienia
Ustawienia::Ustawienia() {
    NOTIFY_CONSTRUCTOR(Ustawienia, Ustawienia(), 0, Default_Ustawienia_Ustawienia_SERIALIZE);
    itsBrama = NULL;
}

Ustawienia::~Ustawienia() {
    NOTIFY_DESTRUCTOR(~Ustawienia, true);
    cleanUpRelations();
}

Brama* Ustawienia::getItsBrama() const {
    return itsBrama;
}

void Ustawienia::setItsBrama(Brama* p_Brama) {
    if(p_Brama != NULL)
        {
            p_Brama->_addItsUstawienia(this);
        }
    _setItsBrama(p_Brama);
}

void Ustawienia::cleanUpRelations() {
    if(itsBrama != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsBrama");
            Brama* current = itsBrama;
            if(current != NULL)
                {
                    current->_removeItsUstawienia(this);
                }
            itsBrama = NULL;
        }
}

void Ustawienia::__setItsBrama(Brama* p_Brama) {
    itsBrama = p_Brama;
    if(p_Brama != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsBrama", p_Brama, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsBrama");
        }
}

void Ustawienia::_setItsBrama(Brama* p_Brama) {
    if(itsBrama != NULL)
        {
            itsBrama->_removeItsUstawienia(this);
        }
    __setItsBrama(p_Brama);
}

void Ustawienia::_clearItsBrama() {
    NOTIFY_RELATION_CLEARED("itsBrama");
    itsBrama = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUstawienia::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsBrama", false, true);
    if(myReal->itsBrama)
        {
            aomsRelations->ADD_ITEM(myReal->itsBrama);
        }
}
//#]

IMPLEMENT_META_P(Ustawienia, Default, Default, false, OMAnimatedUstawienia)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/
