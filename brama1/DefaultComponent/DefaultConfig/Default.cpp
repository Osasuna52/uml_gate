/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Thu, 21, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Brama.h"
//## auto_generated
#include "DetektorKolizji.h"
//## auto_generated
#include "Lampa.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "Naped.h"
//## auto_generated
#include "Odbiornik.h"
//## auto_generated
#include "Pilot.h"
//## auto_generated
#include "Sterownik.h"
//## auto_generated
#include "Ustawienia.h"
//#[ ignore
#define evImpuls_SERIALIZE OM_NO_OP

#define evImpuls_UNSERIALIZE OM_NO_OP

#define evImpuls_CONSTRUCTOR evImpuls()

#define evValidate_SERIALIZE OM_NO_OP

#define evValidate_UNSERIALIZE OM_NO_OP

#define evValidate_CONSTRUCTOR evValidate()

#define evDekoduj_SERIALIZE OM_NO_OP

#define evDekoduj_UNSERIALIZE OM_NO_OP

#define evDekoduj_CONSTRUCTOR evDekoduj()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evStart_SERIALIZE OMADD_SER(stan, x2String((int)myEvent->stan))

#define evStart_UNSERIALIZE OMADD_UNSER(int, stan, OMDestructiveString2X)

#define evStart_CONSTRUCTOR evStart(stan)

#define evKrokRuchu_SERIALIZE OMADD_SER(droga, x2String(myEvent->droga))

#define evKrokRuchu_UNSERIALIZE OMADD_UNSER(float, droga, OMDestructiveString2X)

#define evKrokRuchu_CONSTRUCTOR evKrokRuchu(droga)

#define evSprawdzKolizje_SERIALIZE OM_NO_OP

#define evSprawdzKolizje_UNSERIALIZE OM_NO_OP

#define evSprawdzKolizje_CONSTRUCTOR evSprawdzKolizje()

#define evKolizja_SERIALIZE OM_NO_OP

#define evKolizja_UNSERIALIZE OM_NO_OP

#define evKolizja_CONSTRUCTOR evKolizja()

#define evMigaj_SERIALIZE OM_NO_OP

#define evMigaj_UNSERIALIZE OM_NO_OP

#define evMigaj_CONSTRUCTOR evMigaj()

#define evPauza_SERIALIZE OM_NO_OP

#define evPauza_UNSERIALIZE OM_NO_OP

#define evPauza_CONSTRUCTOR evPauza()

#define evAktywuj_SERIALIZE OM_NO_OP

#define evAktywuj_UNSERIALIZE OM_NO_OP

#define evAktywuj_CONSTRUCTOR evAktywuj()

#define evTest_SERIALIZE OM_NO_OP

#define evTest_UNSERIALIZE OM_NO_OP

#define evTest_CONSTRUCTOR evTest()

#define evSignal_SERIALIZE OM_NO_OP

#define evSignal_UNSERIALIZE OM_NO_OP

#define evSignal_CONSTRUCTOR evSignal()

#define evPrzelacz_SERIALIZE OM_NO_OP

#define evPrzelacz_UNSERIALIZE OM_NO_OP

#define evPrzelacz_CONSTRUCTOR evPrzelacz()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evImpuls()
evImpuls::evImpuls() {
    NOTIFY_EVENT_CONSTRUCTOR(evImpuls)
    setId(evImpuls_Default_id);
}

bool evImpuls::isTypeOf(const short id) const {
    return (evImpuls_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evImpuls, Default, Default, evImpuls())

//## event evValidate()
evValidate::evValidate() {
    NOTIFY_EVENT_CONSTRUCTOR(evValidate)
    setId(evValidate_Default_id);
}

bool evValidate::isTypeOf(const short id) const {
    return (evValidate_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evValidate, Default, Default, evValidate())

//## event evDekoduj()
evDekoduj::evDekoduj() {
    NOTIFY_EVENT_CONSTRUCTOR(evDekoduj)
    setId(evDekoduj_Default_id);
}

bool evDekoduj::isTypeOf(const short id) const {
    return (evDekoduj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDekoduj, Default, Default, evDekoduj())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event evStart(Stany)
//#[ ignore
evStart::evStart(int p_stan) : stan((Stany)p_stan) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}
//#]

evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

evStart::evStart(Stany p_stan) : stan(p_stan) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart(Stany))

//## event evKrokRuchu(float)
evKrokRuchu::evKrokRuchu() {
    NOTIFY_EVENT_CONSTRUCTOR(evKrokRuchu)
    setId(evKrokRuchu_Default_id);
}

evKrokRuchu::evKrokRuchu(float p_droga) : droga(p_droga) {
    NOTIFY_EVENT_CONSTRUCTOR(evKrokRuchu)
    setId(evKrokRuchu_Default_id);
}

bool evKrokRuchu::isTypeOf(const short id) const {
    return (evKrokRuchu_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKrokRuchu, Default, Default, evKrokRuchu(float))

//## event evSprawdzKolizje()
evSprawdzKolizje::evSprawdzKolizje() {
    NOTIFY_EVENT_CONSTRUCTOR(evSprawdzKolizje)
    setId(evSprawdzKolizje_Default_id);
}

bool evSprawdzKolizje::isTypeOf(const short id) const {
    return (evSprawdzKolizje_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSprawdzKolizje, Default, Default, evSprawdzKolizje())

//## event evKolizja()
evKolizja::evKolizja() {
    NOTIFY_EVENT_CONSTRUCTOR(evKolizja)
    setId(evKolizja_Default_id);
}

bool evKolizja::isTypeOf(const short id) const {
    return (evKolizja_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKolizja, Default, Default, evKolizja())

//## event evMigaj()
evMigaj::evMigaj() {
    NOTIFY_EVENT_CONSTRUCTOR(evMigaj)
    setId(evMigaj_Default_id);
}

bool evMigaj::isTypeOf(const short id) const {
    return (evMigaj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evMigaj, Default, Default, evMigaj())

//## event evPauza()
evPauza::evPauza() {
    NOTIFY_EVENT_CONSTRUCTOR(evPauza)
    setId(evPauza_Default_id);
}

bool evPauza::isTypeOf(const short id) const {
    return (evPauza_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPauza, Default, Default, evPauza())

//## event evAktywuj()
evAktywuj::evAktywuj() {
    NOTIFY_EVENT_CONSTRUCTOR(evAktywuj)
    setId(evAktywuj_Default_id);
}

bool evAktywuj::isTypeOf(const short id) const {
    return (evAktywuj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAktywuj, Default, Default, evAktywuj())

//## event evTest()
evTest::evTest() {
    NOTIFY_EVENT_CONSTRUCTOR(evTest)
    setId(evTest_Default_id);
}

bool evTest::isTypeOf(const short id) const {
    return (evTest_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evTest, Default, Default, evTest())

//## event evSignal()
evSignal::evSignal() {
    NOTIFY_EVENT_CONSTRUCTOR(evSignal)
    setId(evSignal_Default_id);
}

bool evSignal::isTypeOf(const short id) const {
    return (evSignal_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSignal, Default, Default, evSignal())

//## event evPrzelacz()
evPrzelacz::evPrzelacz() {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzelacz)
    setId(evPrzelacz_Default_id);
}

bool evPrzelacz::isTypeOf(const short id) const {
    return (evPrzelacz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPrzelacz, Default, Default, evPrzelacz())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
