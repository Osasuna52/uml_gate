/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Odbiornik
//!	Generated Date	: Wed, 20, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Odbiornik.h
*********************************************************************/

#ifndef Odbiornik_H
#define Odbiornik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Odbiornik
#include "Modul.h"
//## link itsPilot
#include "Pilot.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Odbiornik
class Odbiornik : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedOdbiornik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Odbiornik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Odbiornik();
    
    ////    Operations    ////
    
    //## operation Dekodowanie()
    void Dekodowanie();
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Pilot* getItsPilot() const;
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    bool getImpulsOK() const;
    
    //## auto_generated
    void setImpulsOK(bool p_impulsOK);
    
    ////    Attributes    ////

protected :

    bool impulsOK;		//## attribute impulsOK
    
    ////    Relations and components    ////
    
    Pilot itsPilot;		//## link itsPilot
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_4:
    //## statechart_method
    inline bool sendaction_4_IN() const;
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Odbiornik_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_4 = 2,
        sendaction_3 = 3,
        sendaction_2 = 4,
        Aktywny = 5
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedOdbiornik : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Odbiornik, OMAnimatedOdbiornik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_4_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Odbiornik::rootState_IN() const {
    return true;
}

inline bool Odbiornik::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Odbiornik::sendaction_4_IN() const {
    return rootState_subState == sendaction_4;
}

inline bool Odbiornik::sendaction_3_IN() const {
    return rootState_subState == sendaction_3;
}

inline bool Odbiornik::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Odbiornik::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Odbiornik.h
*********************************************************************/
