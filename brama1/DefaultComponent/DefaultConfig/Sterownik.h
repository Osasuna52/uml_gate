/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Thu, 21, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsDetektorKolizji
#include "DetektorKolizji.h"
//## link itsLampa
#include "Lampa.h"
//## link itsNaped
#include "Naped.h"
//## link itsOdbiornik
#include "Odbiornik.h"
//## link itsBrama
class Brama;

//## package Default

//## class Sterownik
class Sterownik : public OMThread, public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Operations    ////
    
    //## operation ustawStan(Stany)
    void ustawStan(const Stany& stan);
    
    ////    Additional operations    ////
    
    //## auto_generated
    float getDroga() const;
    
    //## auto_generated
    void setDroga(float p_droga);
    
    //## auto_generated
    float getPozycja() const;
    
    //## auto_generated
    void setPozycja(float p_pozycja);
    
    //## auto_generated
    float getPozycjaMax() const;
    
    //## auto_generated
    void setPozycjaMax(float p_pozycjaMax);
    
    //## auto_generated
    float getPozycjaMin() const;
    
    //## auto_generated
    void setPozycjaMin(float p_pozycjaMin);
    
    //## auto_generated
    int getRejestrAwarii() const;
    
    //## auto_generated
    void setRejestrAwarii(int p_rejestrAwarii);
    
    //## auto_generated
    Stany getStan() const;
    
    //## auto_generated
    void setStan(Stany p_stan);
    
    //## auto_generated
    Stany getStan0() const;
    
    //## auto_generated
    void setStan0(Stany p_stan0);
    
    //## auto_generated
    bool getSygnalizacja() const;
    
    //## auto_generated
    void setSygnalizacja(bool p_sygnalizacja);
    
    //## auto_generated
    Brama* getItsBrama() const;
    
    //## auto_generated
    void setItsBrama(Brama* p_Brama);
    
    //## auto_generated
    DetektorKolizji* getItsDetektorKolizji() const;
    
    //## auto_generated
    Lampa* getItsLampa() const;
    
    //## auto_generated
    Naped* getItsNaped() const;
    
    //## auto_generated
    Odbiornik* getItsOdbiornik() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    float droga;		//## attribute droga
    
    float pozycja;		//## attribute pozycja
    
    float pozycjaMax;		//## attribute pozycjaMax
    
    float pozycjaMin;		//## attribute pozycjaMin
    
    int rejestrAwarii;		//## attribute rejestrAwarii
    
    Stany stan;		//## attribute stan
    
    Stany stan0;		//## attribute stan0
    
    bool sygnalizacja;		//## attribute sygnalizacja
    
    ////    Relations and components    ////
    
    Brama* itsBrama;		//## link itsBrama
    
    DetektorKolizji itsDetektorKolizji;		//## link itsDetektorKolizji
    
    Lampa itsLampa;		//## link itsLampa
    
    Naped itsNaped;		//## link itsNaped
    
    Odbiornik itsOdbiornik;		//## link itsOdbiornik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsBrama(Brama* p_Brama);
    
    //## auto_generated
    void _setItsBrama(Brama* p_Brama);
    
    //## auto_generated
    void _clearItsBrama();
    
    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    inline bool rootState_isCompleted();
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Zatrzymanie:
    //## statechart_method
    inline bool Zatrzymanie_IN() const;
    
    //## statechart_method
    inline bool Zatrzymanie_isCompleted();
    
    //## statechart_method
    void Zatrzymanie_entDef();
    
    //## statechart_method
    void Zatrzymanie_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Zatrzymanie_handleEvent();
    
    // terminationstate_13:
    //## statechart_method
    inline bool terminationstate_13_IN() const;
    
    // sendaction_12:
    //## statechart_method
    inline bool sendaction_12_IN() const;
    
    // sendaction_11:
    //## statechart_method
    inline bool sendaction_11_IN() const;
    
    // sendaction_10:
    //## statechart_method
    inline bool sendaction_10_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_10_handleEvent();
    
    // Testowanie:
    //## statechart_method
    inline bool Testowanie_IN() const;
    
    // terminationstate_2:
    //## statechart_method
    inline bool terminationstate_2_IN() const;
    
    // sendaction_17:
    //## statechart_method
    inline bool sendaction_17_IN() const;
    
    // sendaction_16:
    //## statechart_method
    inline bool sendaction_16_IN() const;
    
    // sendaction_15:
    //## statechart_method
    inline bool sendaction_15_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus OczekiwanieTakeevImpuls();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    // KrokRuchu:
    //## statechart_method
    inline bool KrokRuchu_IN() const;
    
    // Jazda:
    //## statechart_method
    inline bool Jazda_IN() const;
    
    //## statechart_method
    inline bool Jazda_isCompleted();
    
    //## statechart_method
    void Jazda_entDef();
    
    //## statechart_method
    void Jazda_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Jazda_handleEvent();
    
    // terminationstate_8:
    //## statechart_method
    inline bool terminationstate_8_IN() const;
    
    // sendaction_9:
    //## statechart_method
    inline bool sendaction_9_IN() const;
    
    // sendaction_7:
    //## statechart_method
    inline bool sendaction_7_IN() const;
    
    // sendaction_6:
    //## statechart_method
    inline bool sendaction_6_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus sendaction_6_handleEvent();
    
    // Awaria:
    //## statechart_method
    inline bool Awaria_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Sterownik_Enum {
        OMNonState = 0,
        Zatrzymanie = 1,
        terminationstate_13 = 2,
        sendaction_12 = 3,
        sendaction_11 = 4,
        sendaction_10 = 5,
        Testowanie = 6,
        terminationstate_2 = 7,
        sendaction_17 = 8,
        sendaction_16 = 9,
        sendaction_15 = 10,
        Oczekiwanie = 11,
        KrokRuchu = 12,
        Jazda = 13,
        terminationstate_8 = 14,
        sendaction_9 = 15,
        sendaction_7 = 16,
        sendaction_6 = 17,
        Awaria = 18
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int Zatrzymanie_subState;
    
    int Jazda_subState;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Sterownik, OMAnimatedSterownik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Zatrzymanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_13_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_11_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_10_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Testowanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_17_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_16_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_15_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void KrokRuchu_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_8_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_9_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_7_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_6_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Awaria_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Sterownik::rootState_IN() const {
    return true;
}

inline bool Sterownik::rootState_isCompleted() {
    return ( IS_IN(terminationstate_2) );
}

inline bool Sterownik::Zatrzymanie_IN() const {
    return rootState_subState == Zatrzymanie;
}

inline bool Sterownik::Zatrzymanie_isCompleted() {
    return ( IS_IN(terminationstate_13) );
}

inline bool Sterownik::terminationstate_13_IN() const {
    return Zatrzymanie_subState == terminationstate_13;
}

inline bool Sterownik::sendaction_12_IN() const {
    return Zatrzymanie_subState == sendaction_12;
}

inline bool Sterownik::sendaction_11_IN() const {
    return Zatrzymanie_subState == sendaction_11;
}

inline bool Sterownik::sendaction_10_IN() const {
    return Zatrzymanie_subState == sendaction_10;
}

inline bool Sterownik::Testowanie_IN() const {
    return rootState_subState == Testowanie;
}

inline bool Sterownik::terminationstate_2_IN() const {
    return rootState_subState == terminationstate_2;
}

inline bool Sterownik::sendaction_17_IN() const {
    return rootState_subState == sendaction_17;
}

inline bool Sterownik::sendaction_16_IN() const {
    return rootState_subState == sendaction_16;
}

inline bool Sterownik::sendaction_15_IN() const {
    return rootState_subState == sendaction_15;
}

inline bool Sterownik::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

inline bool Sterownik::KrokRuchu_IN() const {
    return rootState_subState == KrokRuchu;
}

inline bool Sterownik::Jazda_IN() const {
    return rootState_subState == Jazda;
}

inline bool Sterownik::Jazda_isCompleted() {
    return ( IS_IN(terminationstate_8) );
}

inline bool Sterownik::terminationstate_8_IN() const {
    return Jazda_subState == terminationstate_8;
}

inline bool Sterownik::sendaction_9_IN() const {
    return Jazda_subState == sendaction_9;
}

inline bool Sterownik::sendaction_7_IN() const {
    return Jazda_subState == sendaction_7;
}

inline bool Sterownik::sendaction_6_IN() const {
    return Jazda_subState == sendaction_6;
}

inline bool Sterownik::Awaria_IN() const {
    return rootState_subState == Awaria;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
