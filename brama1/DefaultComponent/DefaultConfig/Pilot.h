/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Pilot
//!	Generated Date	: Thu, 21, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Pilot.h
*********************************************************************/

#ifndef Pilot_H
#define Pilot_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Pilot
#include "Modul.h"
//## link itsOdbiornik
class Odbiornik;

//## package Default

//## class Pilot
class Pilot : public OMThread, public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPilot;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Pilot(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Pilot();
    
    ////    Operations    ////
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia();
    
    //## operation zapiszUstawienia(std::string)
    virtual bool zapiszUstawienia(std::string nastawy);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Odbiornik* getItsOdbiornik() const;
    
    //## auto_generated
    void setItsOdbiornik(Odbiornik* p_Odbiornik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Odbiornik* itsOdbiornik;		//## link itsOdbiornik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsOdbiornik(Odbiornik* p_Odbiornik);
    
    //## auto_generated
    void _setItsOdbiornik(Odbiornik* p_Odbiornik);
    
    //## auto_generated
    void _clearItsOdbiornik();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Pilot_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_2 = 2,
        Aktywny = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPilot : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Pilot, OMAnimatedPilot)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Pilot::rootState_IN() const {
    return true;
}

inline bool Pilot::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Pilot::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Pilot::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Pilot.h
*********************************************************************/
