/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Thu, 21, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Sterownik.h"
//## link itsBrama
#include "Brama.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define Default_Sterownik_ustawStan_SERIALIZE aomsmethod->addAttribute("stan", x2String((int)stan));
//#]

//## package Default

//## class Sterownik
Sterownik::Sterownik(IOxfActive* theActiveContext) : pozycja(0.), pozycjaMax(4.), pozycjaMin(0.), rejestrAwarii(0), stan(ZATRZYMANIE), stan0(ZAMYKANIE), sygnalizacja(true) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsDetektorKolizji.setShouldDelete(false);
        }
        {
            itsOdbiornik.setShouldDelete(false);
        }
        {
            itsNaped.setShouldDelete(false);
        }
        {
            itsLampa.setShouldDelete(false);
        }
    }
    itsBrama = NULL;
    initRelations();
    initStatechart();
}

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
    cancelTimeouts();
}

void Sterownik::ustawStan(const Stany& stan) {
    NOTIFY_OPERATION(ustawStan, ustawStan(const Stany&), 1, Default_Sterownik_ustawStan_SERIALIZE);
    //#[ operation ustawStan(Stany)
    this->stan0 = this->stan;
    this->stan = stan;
    //#]
}

float Sterownik::getDroga() const {
    return droga;
}

void Sterownik::setDroga(float p_droga) {
    droga = p_droga;
}

float Sterownik::getPozycja() const {
    return pozycja;
}

void Sterownik::setPozycja(float p_pozycja) {
    pozycja = p_pozycja;
    NOTIFY_SET_OPERATION;
}

float Sterownik::getPozycjaMax() const {
    return pozycjaMax;
}

void Sterownik::setPozycjaMax(float p_pozycjaMax) {
    pozycjaMax = p_pozycjaMax;
}

float Sterownik::getPozycjaMin() const {
    return pozycjaMin;
}

void Sterownik::setPozycjaMin(float p_pozycjaMin) {
    pozycjaMin = p_pozycjaMin;
}

int Sterownik::getRejestrAwarii() const {
    return rejestrAwarii;
}

void Sterownik::setRejestrAwarii(int p_rejestrAwarii) {
    rejestrAwarii = p_rejestrAwarii;
}

Stany Sterownik::getStan() const {
    return stan;
}

void Sterownik::setStan(Stany p_stan) {
    stan = p_stan;
}

Stany Sterownik::getStan0() const {
    return stan0;
}

void Sterownik::setStan0(Stany p_stan0) {
    stan0 = p_stan0;
}

bool Sterownik::getSygnalizacja() const {
    return sygnalizacja;
}

void Sterownik::setSygnalizacja(bool p_sygnalizacja) {
    sygnalizacja = p_sygnalizacja;
}

Brama* Sterownik::getItsBrama() const {
    return itsBrama;
}

void Sterownik::setItsBrama(Brama* p_Brama) {
    _setItsBrama(p_Brama);
}

DetektorKolizji* Sterownik::getItsDetektorKolizji() const {
    return (DetektorKolizji*) &itsDetektorKolizji;
}

Lampa* Sterownik::getItsLampa() const {
    return (Lampa*) &itsLampa;
}

Naped* Sterownik::getItsNaped() const {
    return (Naped*) &itsNaped;
}

Odbiornik* Sterownik::getItsOdbiornik() const {
    return (Odbiornik*) &itsOdbiornik;
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsDetektorKolizji.startBehavior();
    done &= itsLampa.startBehavior();
    done &= itsNaped.startBehavior();
    done &= itsOdbiornik.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Sterownik::initRelations() {
    itsDetektorKolizji._setItsSterownik(this);
    itsLampa._setItsSterownik(this);
    itsNaped._setItsSterownik(this);
    itsOdbiornik._setItsSterownik(this);
}

void Sterownik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    Zatrzymanie_subState = OMNonState;
    Jazda_subState = OMNonState;
    rootState_timeout = NULL;
}

void Sterownik::cleanUpRelations() {
    if(itsBrama != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsBrama");
            itsBrama = NULL;
        }
}

void Sterownik::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Sterownik::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void Sterownik::__setItsBrama(Brama* p_Brama) {
    itsBrama = p_Brama;
    if(p_Brama != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsBrama", p_Brama, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsBrama");
        }
}

void Sterownik::_setItsBrama(Brama* p_Brama) {
    __setItsBrama(p_Brama);
}

void Sterownik::_clearItsBrama() {
    NOTIFY_RELATION_CLEARED("itsBrama");
    itsBrama = NULL;
}

void Sterownik::destroy() {
    itsDetektorKolizji.destroy();
    itsLampa.destroy();
    itsNaped.destroy();
    itsOdbiornik.destroy();
    OMReactive::destroy();
}

void Sterownik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Testowanie");
        rootState_subState = Testowanie;
        rootState_active = Testowanie;
        //#[ state Testowanie.(Entry) 
        stan = ZATRZYMANIE;
        stan0 = ZAMYKANIE;
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Sterownik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Testowanie
        case Testowanie:
        {
            if(IS_EVENT_TYPE_OF(evTest_Default_id))
                {
                    //## transition 3 
                    if(rejestrAwarii != 0)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_STATE_EXITED("ROOT.Testowanie");
                            NOTIFY_STATE_ENTERED("ROOT.Awaria");
                            rootState_subState = Awaria;
                            rootState_active = Awaria;
                            //#[ state Awaria.(Entry) 
                            std::cout << "AWARIA: " << rejestrAwarii << std::endl;
                            //#]
                            rootState_timeout = scheduleTimeout(5000, "ROOT.Awaria");
                            NOTIFY_TRANSITION_TERMINATED("3");
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            NOTIFY_TRANSITION_STARTED("33");
                            NOTIFY_STATE_EXITED("ROOT.Testowanie");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_17");
                            pushNullTransition();
                            rootState_subState = sendaction_17;
                            rootState_active = sendaction_17;
                            //#[ state sendaction_17.(Entry) 
                            itsOdbiornik.GEN(evAktywuj);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("33");
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Awaria
        case Awaria:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("1");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Awaria");
                            NOTIFY_STATE_ENTERED("ROOT.terminationstate_2");
                            rootState_subState = terminationstate_2;
                            rootState_active = terminationstate_2;
                            NOTIFY_TRANSITION_TERMINATED("1");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            res = sendaction_6_handleEvent();
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("13");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_7");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda.sendaction_9");
                    pushNullTransition();
                    Jazda_subState = sendaction_9;
                    rootState_active = sendaction_9;
                    //#[ state Jazda.sendaction_9.(Entry) 
                    itsNaped.GEN(evStart(stan));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("13");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_handleEvent();
                }
        }
        break;
        // State terminationstate_8
        case terminationstate_8:
        {
            res = Jazda_handleEvent();
        }
        break;
        // State sendaction_9
        case sendaction_9:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("14");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_9");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda.terminationstate_8");
                    Jazda_subState = terminationstate_8;
                    rootState_active = terminationstate_8;
                    NOTIFY_TRANSITION_TERMINATED("14");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_handleEvent();
                }
        }
        break;
        // State sendaction_10
        case sendaction_10:
        {
            res = sendaction_10_handleEvent();
        }
        break;
        // State sendaction_11
        case sendaction_11:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("22");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_11");
                    NOTIFY_STATE_ENTERED("ROOT.Zatrzymanie.sendaction_12");
                    pushNullTransition();
                    Zatrzymanie_subState = sendaction_12;
                    rootState_active = sendaction_12;
                    //#[ state Zatrzymanie.sendaction_12.(Entry) 
                    itsDetektorKolizji.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("22");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Zatrzymanie_handleEvent();
                }
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("18");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_12");
                    NOTIFY_STATE_ENTERED("ROOT.Zatrzymanie.terminationstate_13");
                    Zatrzymanie_subState = terminationstate_13;
                    rootState_active = terminationstate_13;
                    NOTIFY_TRANSITION_TERMINATED("18");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Zatrzymanie_handleEvent();
                }
        }
        break;
        // State terminationstate_13
        case terminationstate_13:
        {
            res = Zatrzymanie_handleEvent();
        }
        break;
        // State KrokRuchu
        case KrokRuchu:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 26 
                    if(pozycja > pozycjaMin && pozycja < pozycjaMax)
                        {
                            NOTIFY_TRANSITION_STARTED("25");
                            NOTIFY_TRANSITION_STARTED("26");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.KrokRuchu");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_15");
                            pushNullTransition();
                            rootState_subState = sendaction_15;
                            rootState_active = sendaction_15;
                            //#[ state sendaction_15.(Entry) 
                            itsDetektorKolizji.GEN(evSprawdzKolizje);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("26");
                            NOTIFY_TRANSITION_TERMINATED("25");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("25");
                            NOTIFY_TRANSITION_STARTED("24");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.KrokRuchu");
                            Zatrzymanie_entDef();
                            NOTIFY_TRANSITION_TERMINATED("24");
                            NOTIFY_TRANSITION_TERMINATED("25");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_15
        case sendaction_15:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 28 
                    if(sygnalizacja == false)
                        {
                            NOTIFY_TRANSITION_STARTED("27");
                            NOTIFY_TRANSITION_STARTED("28");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.sendaction_15");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            //#[ state Oczekiwanie.(Entry) 
                            std::cout << stan0 << ", " << stan << std::endl;
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("28");
                            NOTIFY_TRANSITION_TERMINATED("27");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("27");
                            NOTIFY_TRANSITION_STARTED("29");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.sendaction_15");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_16");
                            pushNullTransition();
                            rootState_subState = sendaction_16;
                            rootState_active = sendaction_16;
                            //#[ state sendaction_16.(Entry) 
                            itsLampa.GEN(evKrokRuchu(0.));
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("29");
                            NOTIFY_TRANSITION_TERMINATED("27");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_16
        case sendaction_16:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("30");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_16");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    std::cout << stan0 << ", " << stan << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("30");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_17
        case sendaction_17:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("31");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_17");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    std::cout << stan0 << ", " << stan << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("31");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

void Sterownik::Zatrzymanie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Zatrzymanie");
    pushNullTransition();
    rootState_subState = Zatrzymanie;
    NOTIFY_TRANSITION_STARTED("17");
    NOTIFY_STATE_ENTERED("ROOT.Zatrzymanie.sendaction_10");
    pushNullTransition();
    Zatrzymanie_subState = sendaction_10;
    rootState_active = sendaction_10;
    //#[ state Zatrzymanie.sendaction_10.(Entry) 
    itsNaped.GEN(evStop);
    //#]
    NOTIFY_TRANSITION_TERMINATED("17");
}

void Sterownik::Zatrzymanie_exit() {
    popNullTransition();
    switch (Zatrzymanie_subState) {
        // State sendaction_10
        case sendaction_10:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_10");
        }
        break;
        // State sendaction_11
        case sendaction_11:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_11");
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_12");
        }
        break;
        // State terminationstate_13
        case terminationstate_13:
        {
            NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.terminationstate_13");
        }
        break;
        default:
            break;
    }
    Zatrzymanie_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Zatrzymanie");
}

IOxfReactive::TakeEventStatus Sterownik::Zatrzymanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 11 
            if(IS_COMPLETED(Zatrzymanie)==true)
                {
                    NOTIFY_TRANSITION_STARTED("11");
                    Zatrzymanie_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    std::cout << stan0 << ", " << stan << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("11");
                    res = eventConsumed;
                }
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_10_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 21 
            if(sygnalizacja == false)
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    NOTIFY_TRANSITION_STARTED("21");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_10");
                    NOTIFY_STATE_ENTERED("ROOT.Zatrzymanie.sendaction_12");
                    pushNullTransition();
                    Zatrzymanie_subState = sendaction_12;
                    rootState_active = sendaction_12;
                    //#[ state Zatrzymanie.sendaction_12.(Entry) 
                    itsDetektorKolizji.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("21");
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
            else
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    NOTIFY_TRANSITION_STARTED("20");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zatrzymanie.sendaction_10");
                    NOTIFY_STATE_ENTERED("ROOT.Zatrzymanie.sendaction_11");
                    pushNullTransition();
                    Zatrzymanie_subState = sendaction_11;
                    rootState_active = sendaction_11;
                    //#[ state Zatrzymanie.sendaction_11.(Entry) 
                    itsLampa.GEN(evStop);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("20");
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = Zatrzymanie_handleEvent();
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::OczekiwanieTakeevImpuls() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    //## transition 6 
    if(stan == ZATRZYMANIE && stan0 == OTWIERANIE)
        {
            NOTIFY_TRANSITION_STARTED("5");
            NOTIFY_TRANSITION_STARTED("6");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 6 
            ustawStan(ZAMYKANIE);
            //#]
            Jazda_entDef();
            NOTIFY_TRANSITION_TERMINATED("6");
            NOTIFY_TRANSITION_TERMINATED("5");
            res = eventConsumed;
        }
    else
        {
            //## transition 7 
            if(stan == ZATRZYMANIE && stan0 == ZAMYKANIE)
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    NOTIFY_TRANSITION_STARTED("7");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    //#[ transition 7 
                    ustawStan(OTWIERANIE);
                    //#]
                    Jazda_entDef();
                    NOTIFY_TRANSITION_TERMINATED("7");
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            else
                {
                    //## transition 8 
                    if(stan != ZATRZYMANIE)
                        {
                            NOTIFY_TRANSITION_STARTED("5");
                            NOTIFY_TRANSITION_STARTED("8");
                            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                            //#[ transition 8 
                            ustawStan(ZATRZYMANIE);
                            //#]
                            Zatrzymanie_entDef();
                            NOTIFY_TRANSITION_TERMINATED("8");
                            NOTIFY_TRANSITION_TERMINATED("5");
                            res = eventConsumed;
                        }
                }
        }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evKolizja_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("32");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 32 
            std::cout << "kolizja detected" << std::endl;
            //#]
            Zatrzymanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("32");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKrokRuchu_Default_id))
        {
            OMSETPARAMS(evKrokRuchu);
            NOTIFY_TRANSITION_STARTED("23");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.KrokRuchu");
            pushNullTransition();
            rootState_subState = KrokRuchu;
            rootState_active = KrokRuchu;
            //#[ state KrokRuchu.(Entry) 
            droga = params->droga;
            pozycja += droga;
            std::cout << "droga= " << droga << ", pozycja= " << pozycja << std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("23");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evImpuls_Default_id))
        {
            res = OczekiwanieTakeevImpuls();
        }
    
    return res;
}

void Sterownik::Jazda_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Jazda");
    pushNullTransition();
    rootState_subState = Jazda;
    NOTIFY_TRANSITION_STARTED("9");
    NOTIFY_STATE_ENTERED("ROOT.Jazda.sendaction_6");
    pushNullTransition();
    Jazda_subState = sendaction_6;
    rootState_active = sendaction_6;
    //#[ state Jazda.sendaction_6.(Entry) 
    itsDetektorKolizji.GEN(evStart(stan));
    //#]
    NOTIFY_TRANSITION_TERMINATED("9");
}

void Sterownik::Jazda_exit() {
    popNullTransition();
    switch (Jazda_subState) {
        // State sendaction_6
        case sendaction_6:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_6");
        }
        break;
        // State sendaction_7
        case sendaction_7:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_7");
        }
        break;
        // State terminationstate_8
        case terminationstate_8:
        {
            NOTIFY_STATE_EXITED("ROOT.Jazda.terminationstate_8");
        }
        break;
        // State sendaction_9
        case sendaction_9:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_9");
        }
        break;
        default:
            break;
    }
    Jazda_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Jazda");
}

IOxfReactive::TakeEventStatus Sterownik::Jazda_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 16 
            if(IS_COMPLETED(Jazda)==true)
                {
                    NOTIFY_TRANSITION_STARTED("16");
                    Jazda_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    std::cout << stan0 << ", " << stan << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("16");
                    res = eventConsumed;
                }
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::sendaction_6_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 15 
            if(sygnalizacja == false)
                {
                    NOTIFY_TRANSITION_STARTED("10");
                    NOTIFY_TRANSITION_STARTED("15");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda.sendaction_9");
                    pushNullTransition();
                    Jazda_subState = sendaction_9;
                    rootState_active = sendaction_9;
                    //#[ state Jazda.sendaction_9.(Entry) 
                    itsNaped.GEN(evStart(stan));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("15");
                    NOTIFY_TRANSITION_TERMINATED("10");
                    res = eventConsumed;
                }
            else
                {
                    NOTIFY_TRANSITION_STARTED("10");
                    NOTIFY_TRANSITION_STARTED("12");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda.sendaction_7");
                    pushNullTransition();
                    Jazda_subState = sendaction_7;
                    rootState_active = sendaction_7;
                    //#[ state Jazda.sendaction_7.(Entry) 
                    itsLampa.GEN(evStart(stan));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("12");
                    NOTIFY_TRANSITION_TERMINATED("10");
                    res = eventConsumed;
                }
        }
    
    if(res == eventNotConsumed)
        {
            res = Jazda_handleEvent();
        }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("stan", x2String((int)myReal->stan));
    aomsAttributes->addAttribute("stan0", x2String((int)myReal->stan0));
    aomsAttributes->addAttribute("sygnalizacja", x2String(myReal->sygnalizacja));
    aomsAttributes->addAttribute("rejestrAwarii", x2String(myReal->rejestrAwarii));
    aomsAttributes->addAttribute("pozycja", x2String(myReal->pozycja));
    aomsAttributes->addAttribute("droga", x2String(myReal->droga));
    aomsAttributes->addAttribute("pozycjaMin", x2String(myReal->pozycjaMin));
    aomsAttributes->addAttribute("pozycjaMax", x2String(myReal->pozycjaMax));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsBrama", false, true);
    if(myReal->itsBrama)
        {
            aomsRelations->ADD_ITEM(myReal->itsBrama);
        }
    aomsRelations->addRelation("itsDetektorKolizji", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetektorKolizji);
    aomsRelations->addRelation("itsOdbiornik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsOdbiornik);
    aomsRelations->addRelation("itsNaped", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsNaped);
    aomsRelations->addRelation("itsLampa", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsLampa);
}

void OMAnimatedSterownik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Sterownik::Testowanie:
        {
            Testowanie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Awaria:
        {
            Awaria_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_2:
        {
            terminationstate_2_serializeStates(aomsState);
        }
        break;
        case Sterownik::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Jazda:
        {
            Jazda_serializeStates(aomsState);
        }
        break;
        case Sterownik::Zatrzymanie:
        {
            Zatrzymanie_serializeStates(aomsState);
        }
        break;
        case Sterownik::KrokRuchu:
        {
            KrokRuchu_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_15:
        {
            sendaction_15_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_16:
        {
            sendaction_16_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_17:
        {
            sendaction_17_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::Zatrzymanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zatrzymanie");
    switch (myReal->Zatrzymanie_subState) {
        case Sterownik::sendaction_10:
        {
            sendaction_10_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_11:
        {
            sendaction_11_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_12:
        {
            sendaction_12_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_13:
        {
            terminationstate_13_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_13_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zatrzymanie.terminationstate_13");
}

void OMAnimatedSterownik::sendaction_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zatrzymanie.sendaction_12");
}

void OMAnimatedSterownik::sendaction_11_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zatrzymanie.sendaction_11");
}

void OMAnimatedSterownik::sendaction_10_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zatrzymanie.sendaction_10");
}

void OMAnimatedSterownik::Testowanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Testowanie");
}

void OMAnimatedSterownik::terminationstate_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.terminationstate_2");
}

void OMAnimatedSterownik::sendaction_17_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_17");
}

void OMAnimatedSterownik::sendaction_16_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_16");
}

void OMAnimatedSterownik::sendaction_15_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_15");
}

void OMAnimatedSterownik::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}

void OMAnimatedSterownik::KrokRuchu_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.KrokRuchu");
}

void OMAnimatedSterownik::Jazda_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda");
    switch (myReal->Jazda_subState) {
        case Sterownik::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_7:
        {
            sendaction_7_serializeStates(aomsState);
        }
        break;
        case Sterownik::terminationstate_8:
        {
            terminationstate_8_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_9:
        {
            sendaction_9_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::terminationstate_8_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda.terminationstate_8");
}

void OMAnimatedSterownik::sendaction_9_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda.sendaction_9");
}

void OMAnimatedSterownik::sendaction_7_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda.sendaction_7");
}

void OMAnimatedSterownik::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda.sendaction_6");
}

void OMAnimatedSterownik::Awaria_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Awaria");
}
//#]

IMPLEMENT_REACTIVE_META_P(Sterownik, Default, Default, false, OMAnimatedSterownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
