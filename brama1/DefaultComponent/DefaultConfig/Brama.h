/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Brama
//!	Generated Date	: Wed, 20, May 2020  
	File Path	: DefaultComponent/DefaultConfig/Brama.h
*********************************************************************/

#ifndef Brama_H
#define Brama_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
#include <oxf/omlist.h>
//## link itsSterownik
#include "Sterownik.h"
//## link itsUstawienia
class Ustawienia;

//## package Default

//## class Brama
class Brama : public OMThread, public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedBrama;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Brama(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Brama();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    OMIterator<Ustawienia*> getItsUstawienia() const;
    
    //## auto_generated
    Ustawienia* newItsUstawienia();
    
    //## auto_generated
    void deleteItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik itsSterownik;		//## link itsSterownik
    
    OMList<Ustawienia*> itsUstawienia;		//## link itsUstawienia
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void _addItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    void _removeItsUstawienia(Ustawienia* p_Ustawienia);
    
    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // sendaction_4:
    //## statechart_method
    inline bool sendaction_4_IN() const;
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // sendaction_1:
    //## statechart_method
    inline bool sendaction_1_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Brama_Enum {
        OMNonState = 0,
        sendaction_4 = 1,
        sendaction_3 = 2,
        sendaction_2 = 3,
        sendaction_1 = 4,
        Oczekiwanie = 5
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedBrama : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Brama, OMAnimatedBrama)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_4_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_1_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Brama::rootState_IN() const {
    return true;
}

inline bool Brama::sendaction_4_IN() const {
    return rootState_subState == sendaction_4;
}

inline bool Brama::sendaction_3_IN() const {
    return rootState_subState == sendaction_3;
}

inline bool Brama::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Brama::sendaction_1_IN() const {
    return rootState_subState == sendaction_1;
}

inline bool Brama::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Brama.h
*********************************************************************/
